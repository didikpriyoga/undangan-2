import React from 'react';
import ImageGallery from 'react-image-gallery';
import 'react-image-gallery/styles/css/image-gallery.css';
import { photos } from './photo-data';

function PhotoSection() {
  // const renderYoutubeVideo = () => {
  //   return (
  //     <iframe
  //       title="Pre-Wedding Didik & Ayu Shabrina"
  //       width="100%"
  //       height="360px"
  //       src="https://www.youtube.com/embed/75w39OY7N-k"
  //       frameBorder="0"
  //       allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
  //       allowFullScreen
  //     ></iframe>
  //   );
  // };

  return (
    <div id="fh5co-testimonial" className="fh5co-section-gray">
      <div className="container">
        <div className="row">
          <div className="row">
            <div className="col-md-8 col-md-offset-2 text-center fh5co-heading">
              <h2 className="main-font">#Didik_AyuShabrina_Gallery</h2>
              <div className="row">
                <div className="col-md-10 col-md-offset-1">
                  <ImageGallery items={photos} showBullets={false} />
                </div>
              </div>
              <p className="sub-title" style={{ marginTop: 16 }}>
                "Dan di antara tanda-tanda (kebesaran)-Nya ialah Dia menciptakan pasangan-pasangan untukmu dari jenismu sendiri, agar kamu cenderung dan merasa tenteram kepadanya, dan Dia menjadikan di antaramu rasa kasih dan sayang. Sungguh, pada yang demikian itu benar-benar terdapat tanda-tanda (kebesaran Allah) bagi kaum yang berpikir."
                (QS. Ar-Rum : 21)
              </p>
            </div>
          </div>
          {/* <div className="row">
            <div className="col-md-10 col-md-offset-1">{renderYoutubeVideo()}</div>
          </div> */}

        </div>
      </div>
    </div>
  );
}

export default PhotoSection;
