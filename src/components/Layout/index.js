import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';
import { node } from 'prop-types';

import Favicon from '@assets/images/didik-ayu.png';
import '@assets/css/icomoon.css';
import '@assets/css/bootstrap.css';
import '@assets/css/style.css';

const IMAGE_URL = `https://didik-ayushabrina.site/static/slide-6-4715e29302dbaa2ba21494c6258298d4.jpg`;
const META_DESCRIPTION = `Assalamualaikum Wr. Wb. Mahasuci Allah telah menciptakan makhluk hidupdengan berpasangpasangan. Begitu pula manusia. Sungguh besar rahmat dan karunia yang diberikan-Nya kepada kita semua. Izinkan kami mengundang sekaligus mengharapkan doa restu dari bapak/ibu dan saudara/i dalam acara pernikahan kami. - Didik & Ayu Shabrina`;
function MainLayout({ children }) {
  return (
    <Fragment>
      <Helmet>
        <title>Didik ❤️ Ayu Shabrina Wedding</title>

        {/* Favicon */}
        <link rel="icon" type="image/png" href={Favicon} />

        {/* font and SEO tags */}
        <meta property="og:title" content="The Wedding of Didik & Ayu Shabrina" />
        <meta property="og:image" content={IMAGE_URL} />
        <meta property="og:url" content="https://didik-ayushabrina.site" />
        <meta property="og:site_name" content="The Wedding of Didik & Ayu Shabrina" />
        <meta property="og:description" content={META_DESCRIPTION} />
        <meta name="twitter:title" content="The Wedding of Didik & Ayu Shabrina" />
        <meta name="twitter:description" content={META_DESCRIPTION} />
        <meta name="twitter:image" content={IMAGE_URL} />
        <meta name="twitter:url" content="https://didik-ayushabrina.site" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:creator" content="@idindrakusuma" />

        <link
          href="https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700"
          rel="stylesheet"
          type="text/css"
        />
        <link href="https://fonts.googleapis.com/css2?family=Cookie&display=swap" rel="stylesheet"></link>
      </Helmet>
      <div id="page">{children}</div>
    </Fragment>
  );
}

MainLayout.propTypes = {
  children: node.isRequired,
};

export default MainLayout;
