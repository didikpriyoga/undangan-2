import React, { Fragment } from 'react';
import { GOOGLE_MAPS_LINK_BRIDE,GOOGLE_MAPS_LINK_GROOM, } from '@/constants';
import { styWrapper } from './styles';

function LocationSection() {
  return (
    <Fragment>
      <div id="fh5co-couple-story" className="fh5co-section-gray" css={styWrapper}>
        <div className="container">
          <div className="row">
            <div className="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
              <h2 className="main-font">Lokasi Acara</h2>
              <p className="sub-title">
                <a
                  href={GOOGLE_MAPS_LINK_GROOM}
                  title="Click untuk melihat peta di Google Maps"
                  target="_blank"
                  rel="noreferrer"
                  style={{ color: '#828282' }}
                >
                  <strong>Rumah Mempelai Putra</strong>
                </a>{' '}
                <br />
                Pancurendang, RT 05 RW 05. <br />
                Ajibarang, Banyumas, Jawa Tengah.
              </p>
            </div>
          </div>
          <div className="row">
            <div className="col-md-10 col-md-offset-1">
              <iframe
                src="https://maps.google.com/maps?q=-7.413178,%20109.099179&t=&z=15&ie=UTF8&iwloc=&output=embed"
                width="100%"
                height="450"
                frameBorder="0"
                style={{ border: '0' }}
                allowFullScreen
                aria-hidden="false"
                tabIndex="0"
                title="Google Maps - Didik House"
              ></iframe>
            </div>
          </div>
          <div className="row">
            <div className="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box" style={{marginTop: 24}}>
              <p className="sub-title">
                <a
                  href={GOOGLE_MAPS_LINK_BRIDE}
                  title="Click untuk melihat peta di Google Maps"
                  target="_blank"
                  rel="noreferrer"
                  style={{ color: '#828282' }}
                >
                  <strong>Rumah Mempelai Putri</strong>
                </a>{' '}
                <br />
                Munggangsari, Lesmana, RT 02 RW 09. <br />
                Ajibarang, Banyumas, Jawa Tengah.
              </p>
            </div>
          </div>
          <div className="row">
            <div className="col-md-10 col-md-offset-1">
              <iframe
                src="https://maps.google.com/maps?q=-7.406792,%20109.087603&t=&z=17&ie=UTF8&iwloc=&output=embed"
                width="100%"
                height="450"
                frameBorder="0"
                style={{ border: '0' }}
                allowFullScreen
                aria-hidden="false"
                tabIndex="0"
                title="Google Maps - Didik & Ayu Shabrina Wedding Party"
              ></iframe>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default React.memo(LocationSection);
