import React from 'react';

import SectionBox from './SectionBox';

import logoBRI from './assets/logoBRI.jpg'
import logoLinkAja from './assets/logoLinkAja.jpg'
import logoDana from './assets/logoDana.jpg'
import logoOvo from './assets/logoOvo.jpg'

function EnvelopeSection() {
  return (
    <div id="fh5co-couple">
      <div className="container">
        <div className="row">
          <div className="col-md-8 col-md-offset-2 text-center fh5co-heading">
            <h2 className="main-font">Amplop Undangan</h2>
            <p className="info" style={{ marginBottom: '-16px' }}>
              Bagi yang tidak dapat hadir pada acara kami dan ingin mengirimkan amplop bisa melalui:
            </p>
          </div>
        </div>
        {/* BOX INFO */}
        <div className="row">
          <div className="col-md-12">
            <SectionBox icon={logoBRI} number="6601-0101-3966-538" text="DIDIK PRIYOGA" />
            <SectionBox icon={logoLinkAja} number="0851-5650-8022" text="DIDIK PRIYOGA" />
            <SectionBox icon={logoDana} number="0851-5650-8022" text="DNID DIDIK PRIYOGA" />
            <SectionBox icon={logoDana} number="0895-3383-28593" text="DNID FILDZAH AYU SHABRINA" />
            <SectionBox icon={logoOvo} number="0895-3383-28593" text="OVO FILDZAH AYU SHABRINA" />
          </div>
        </div>
        <p className="info">
          *Diharapkan untuk melakukan konfirmasi ke nomor tersebut melalui Whatsapp
        </p>
      </div>
    </div>
  );
}

export default EnvelopeSection;
