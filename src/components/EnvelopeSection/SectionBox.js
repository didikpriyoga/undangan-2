import React from 'react';
import { styBoxWrapper } from './styles';

function SectionBox({ icon, number, text }) {
  return (
    <div className="col-md-4" css={styBoxWrapper}>
      <div className="img-section" style={{ marginRight: 16 }}>
        <img src={icon} alt="icon" className="img" />
      </div>
      <div className="text__section">
        <span className="text__info">{number}</span><br></br>
        <span className="text__info">{text}</span>
      </div>
    </div>
  );
}

export default SectionBox;
