import React, { useState, Fragment, useEffect } from 'react';
import { bool } from 'prop-types';
import { styWrapper } from '../HelloSection/styles';
import { Element } from 'react-scroll'

function RSVPSection({ isInvitation }) {
    const [name, setName] = useState('');
    const [wish, setWish] = useState('');
    const [rsvp, setRsvp] = useState('');
    const [datas, setDatas] = useState([])
    const [loading, setLoading] = useState(false)
    const handleSetName = (e) => {
        setName(e.target.value);
    };
    const handleSetWishes = (e) => {
        setWish(e.target.value);
    };

    const handleRsvp = (e) => {
        setRsvp(e.target.value)
    }

    useEffect(() => {
        fetchData()
    }, []);

    async function fetchData() {
        try {
            const url = 'https://api.didik-ayushabrina.site/show'
            const response = await fetch(`${url}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            const json = await response.json();
            setDatas(json.data)
            setLoading(false)
            setName('')
            setWish('')
            setRsvp('')
        } catch (error) {
        }
    }

    async function insertData(){
        try {
            const params ={
                name, wish, rsvp
            }

            const url = 'https://api.didik-ayushabrina.site/create'
            const response = await fetch(`${url}`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            const json = await response.json();
        } catch (error) {
        }
    }

    const handleSubmitButton = async (e) => {
        if (name !== '' && wish !== '' && rsvp !== '') {
            await setLoading(true)
            await insertData()
            await fetchData()
        } else {
        }
    };

    return (
        <>
            <Fragment>
                {!isInvitation && (
                    <div id="fh5co-couple" css={styWrapper}>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-8 col-md-offset-2 text-center fh5co-heading">
                                    <h2 className="main-font">RSVP</h2>
                                    <p className="info">
                                        Konfirmasikan kehadiran anda pada form berikut ini:
                                    </p>
                                    <div class="form-group">
                                        <label htmlFor="inputName" style={{ marginTop: 16 }}>Nama</label>
                                        <input
                                            value={name}
                                            onChange={handleSetName}
                                            type="text"
                                            className="form-control"
                                            placeholder="Masukkan Nama.."
                                        ></input>
                                        <label htmlFor="inputWishes" style={{ marginTop: 16 }}>Ucapan dan Doa</label>
                                        <input
                                            value={wish}
                                            onChange={handleSetWishes}
                                            type="textarea"
                                            className="form-control"
                                            placeholder="Masukkan ucapan dan doa untuk kami..."
                                            aria-multiline
                                        ></input>
                                        <div style={{ flexDirection: 'column' }}>
                                            <label htmlFor="inputRsvp" style={{ marginTop: 16 }}>Konfirmasi Kehadiran</label>
                                            <div style={{ marginLeft: -16 }}>
                                                <input type="radio" name="rsvp"
                                                    style={{ margin: 16, width: 20, height: 20, marginTop: 20 }}
                                                    value={'HADIR'}
                                                    checked={rsvp === 'HADIR'}
                                                    onChange={handleRsvp} />{'HADIR'}
                                                <input type="radio" name="rsvp"
                                                    style={{ margin: 16, width: 20, height: 20, marginTop: 20 }}
                                                    value={'TIDAK'}
                                                    size={20}
                                                    checked={rsvp === 'TIDAK'}
                                                    onChange={handleRsvp} />{'TIDAK'}
                                            </div>

                                        </div>

                                    </div>
                                    <button type="submit" class="btn btn-primary" onClick={handleSubmitButton}>
                                        {!loading ? 'Submit' : 'Proses Submit...'}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </Fragment>
            <div id="fh5co-couple" className="fh5co-section-gray">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 col-md-offset-2 text-center fh5co-heading">
                            <h2 className="main-font">Ucapan dan Doa</h2>
                            <div style={{ backgroundColor: '#fff' }}>

                                <Element name="test7" className="element" id="containerElement" style={{
                                    position: 'relative',
                                    height: '400px',
                                    overflow: 'scroll',
                                    marginBottom: '16px',

                                }}>
                                    {
                                        loading &&
                                        <Element>
                                            Loading...
                                        </Element>
                                    }
                                    {!loading && datas.map((item) => {
                                        return (
                                            <>
                                                <Element name="firstInsideContainer" style={{
                                                    marginTop: '4px',
                                                    marginBottom: '4px',
                                                    fontWeight: 'bold'
                                                }}>
                                                    {item.name}
                                                </Element>
                                                <Element name="firstInsideContainer" style={{
                                                    marginBottom: '16px',
                                                }}>
                                                    {item.wish}
                                                </Element>
                                            </>
                                        )
                                    })}

                                </Element>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

RSVPSection.propTypes = {
    isInvitation: bool.isRequired,
};

export default React.memo(RSVPSection);
