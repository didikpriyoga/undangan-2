import React, { Fragment } from 'react';
import { bool } from 'prop-types';

import WeddingInfoBox from './WeddingInfoBox';
import { styWrapper } from './styles';

function WeddingSection({ isInvitation }) {
  const renderGuestInfo = () => {
    return (
      <Fragment>
        <div className="col-md-10 col-md-offset-1">
          <WeddingInfoBox
            title="Syukuran Pernikahan"
            time="08.00 WIB - Selesai"
            date="Sabtu-Minggu, 20-21 November 2021"
            description="Rumah Mempelai Putra dan Putri"
          />
          <WeddingInfoBox
            title="Akad Nikah & Resepsi"
            time="09.00 WIB - Selesai"
            date="Senin, 22 November 2021"
            description="Rumah Mempelai Putri"
          />
        </div>
        {/* <ButtonLive /> */}
      </Fragment>
    );
  };

  return (
    <Fragment>
      <div id="fh5co-event" css={styWrapper}>
        <div className="overlay" />
        <div className="container">
          <div className="row">
            <div className="col-md-8 col-md-offset-2 text-center fh5co-heading">
              {/* <span className="bismillah">بِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيم</span> */}
              <h2 className="main-font main-font__wedding">Acara Pernikahan</h2>
              <span className="sub-title sub-title__wedding">Insha Allah akan diselenggarakan pada:</span>
            </div>
          </div>
          <div className="row">
            {!isInvitation && renderGuestInfo()}
            {isInvitation && (
              <div className="col-md-10 col-md-offset-1">
                <WeddingInfoBox
                  title="Syukuran Pernikahan"
                  time="08.00 WIB - Selesai"
                  date="Sabtu-Minggu, 20-21 November 2021"
                  description="Rumah Mempelai Putra dan Putri"
                />
                <WeddingInfoBox
                  title="Akad Nikah & Resepsi"
                  time="09.00 WIB - Selesai"
                  date="Senin, 22 November 2021"
                  description="Rumah Mempelai Putri"
                />
              </div>
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
}

WeddingSection.propTypes = {
  isInvitation: bool.isRequired,
};

export default React.memo(WeddingSection);
