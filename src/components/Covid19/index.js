import React from 'react';

import IconMask from './assets/face-mask.png';
import IconSocialDistancing from './assets/social-distancing.png';
import IconHandWashing from './assets/hand-washing.png';
import SectionBox from './SectionBox';

function CovidSection() {
  return (
    <div id="fh5co-couple">
      <div className="container">
        <div className="row">
          <div className="col-md-8 col-md-offset-2 text-center fh5co-heading">
            <h2 className="main-font">Protokol Kesehatan</h2>
            <p className="info" style={{ marginBottom: '-16px' }}>
              Demi menjaga keberlangsungan acara ini dan menjaga kesehatan kita semua, untuk para tamu diharapkan:
            </p>
          </div>
        </div>
        {/* BOX INFO */}
        <div className="row">
          <div className="col-md-12">
            <SectionBox icon={IconHandWashing} text="Mencuci Tangan & Memakai Hand Sanitizer" />
            <SectionBox icon={IconMask} text="Wajib menggunakan masker" />
            <SectionBox icon={IconSocialDistancing} text="Saling menjaga jarak antar tamu" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default CovidSection;
