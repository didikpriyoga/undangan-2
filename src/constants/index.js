/**
 * Link
 */
export const GOOGLE_CALENDAR_LINK = `https://calendar.google.com/calendar/event?action=TEMPLATE&dates=20201003T040000Z%2F20201003T060000Z&text=Dinda+%26+Indra+Wedding&details=Dinda+%26+Indra+Wedding`;
export const GOOGLE_MAPS_LINK_BRIDE = `https://qr.page/g/uT830NxZu`;
export const GOOGLE_MAPS_LINK_GROOM = `https://qr.page/g/uT830NxZu`;

/**
 * Wedding time
 */
export const EPOCH_START_EVENT = 1637550000;
export const EPOCH_END_EVENT = 1637571600;
export const UTC_WEDDING_TIME = '2021-11-22:03:00:00Z';
